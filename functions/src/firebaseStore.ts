import config from "./config";
const firebase = require('firebase');
require('firebase/firestore');

firebase.initializeApp(config);

const settings = {
  timestampsInSnapshots: true
};

const db = firebase.firestore();
db.settings(settings);

export default db;
import * as functions from 'firebase-functions';
import api from "./app";

exports.api = functions.https.onRequest(api);
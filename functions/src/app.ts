
const express = require('express');
const cors = require('cors');

import user from './user';
import categories from './categories';
import tasks from "./tasks";
import notes from "./notes";

const app = express();

// Automatically allow cross-origin requests
app.use(cors({
  origin: true
}));

app.use('/user', user);
app.use('/categories', categories);
app.use('/tasks', tasks);
app.use('/notes', notes);

export default app;
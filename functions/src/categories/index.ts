const express = require('express');
import db from "../firebaseStore";

const app = express.Router();

import createCategories from "./createCategories";
import getAllCategories from "./getAllCategories";
import updateCategories from "./updateCategories";

app.get('/all/:userId', (req, res) => getAllCategories(db, req, res));
app.post('/', (req, res) => createCategories(db, req, res));
app.put('/:id', (req, res) => updateCategories(db, req, res));

export default app;
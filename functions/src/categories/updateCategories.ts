import {
  updateSchema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.params
  }, updateSchema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    id,
  } = req.params;

  const updatedBody = { ...req.body, updateAt: Date.now() };
  db.collection("categories")
    .doc(id)
    .update(updatedBody)
    .then(updatedCategories => res.status(200).send({updatedCategories}))
}
import {
  createShema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.body
  }, createShema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    name,
    userId,
    favorite,
    hide,
    points,
  } = req.body

  db.collection("categories")
    .add({
      name,
      userId,
      favorite: favorite ? favorite : false,
      hide: hide ? hide : false,
      points: points ? points : 0,
      createdAt: Date.now(),
      updateAt: Date.now(),
    })
    .then(category => res.status(201).send(category));
}
const Joi = require('joi');

const createShema = Joi.object().keys({
  name: Joi.string().alphanum().max(30).required(),
  userId: Joi.string().required(),
  categoryId: Joi.string().required(),
  favorite: Joi.bool(),
  done: Joi.bool(),
  rewards: Joi.bool(),
  deadline: Joi.number(),
  hide: Joi.bool(),
  points: Joi.number(),
});

const getAllSchema = Joi.object().keys({
  userId: Joi.string().required()
});

const updateSchema = Joi.object().keys({
  id: Joi.string().alphanum().required(),
  name: Joi.string().alphanum().max(30).required(),
  userId: Joi.string().required(),
  favorite: Joi.bool(),
  done: Joi.bool(),
  rewards: Joi.bool(),
  deadline: Joi.number(),
  hide: Joi.bool(),
  points: Joi.number(),
});

const validateHelper = (data, schema, cb) => Joi.validate({ ...data }, schema, cb);

export {
  createShema,
  updateSchema,
  getAllSchema,
  validateHelper
}
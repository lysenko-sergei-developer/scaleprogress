import {
  getAllSchema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.params }, getAllSchema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    userId,
  } = req.params

  db.collection("tasks")
    .where("userId", "==", userId)
    .where("hide", "==", false)
    .get()
    .then((querySnapshot) => {
      const data = [];
      querySnapshot.forEach(doc => data.push({
        id: doc.id,
        ...doc.data()
      }));
      res.status(200).send(data);
    });
}
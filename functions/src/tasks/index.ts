const express = require('express');
import db from "../firebaseStore";

const app = express.Router();

import createTask from "./createTask";
import getAllTasks from "./getAllTasks";
import updateCTasks from "./updateTask";

app.get('/all/:userId', (req, res) => getAllTasks(db, req, res));
app.post('/', (req, res) => createTask(db, req, res));
app.put('/:id', (req, res) => updateCTasks(db, req, res));

export default app;
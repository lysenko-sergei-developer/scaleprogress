import {
  createShema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.body
  }, createShema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    name,
    userId,
    categoryId,
    favorite,
    hide,
    points,
    done,
    rewards,
    deadline
  } = req.body

  db.collection("tasks")
    .add({
      name,
      userId,
      categoryId,
      favorite: favorite ? favorite : false,
      hide: hide ? hide : false,
      done: done ? done : false,
      rewards: rewards ? rewards : false,
      deadline: deadline ? deadline : 0,
      points: points ? points : 0,
      createdAt: Date.now(),
      updateAt: Date.now(),
    })
    .then(task => res.status(201).send(task))
    .catch(err => res.status(500).send(err));
}
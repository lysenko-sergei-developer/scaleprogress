import {
  createShema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.body
  }, createShema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    id,
  } = req.params;

  const updatedBody = { ...req.body, updateAt: Date.now() };
  db.collection("notes")
    .doc(id)
    .update(updatedBody)
    .then(updatedNotes => res.status(200).send({updatedNotes}))
}
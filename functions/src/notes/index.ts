const express = require('express');
import db from "../firebaseStore";

const app = express.Router();

import createNotes from "./createNotes";
import getLastNotes from "./getLastNotes";
import updateNotes from "./updateNotes";

app.get('/', (req, res) => getLastNotes(db, req, res));
app.post('/', (req, res) => createNotes(db, req, res));
app.put('/:id', (req, res) => updateNotes(db, req, res));

export default app;
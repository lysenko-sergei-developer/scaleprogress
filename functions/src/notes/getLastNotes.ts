import {
  getLastSchema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.params }, getLastSchema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    userId,
  } = req.params

  db.collection("notes")
    .where("userId", "==", userId)
    .get()
    .then((querySnapshot) => {
      const data = [];
      querySnapshot.forEach(doc => data.push({
        id: doc.id,
        ...doc.data()
      }));
      const dataLast = data[0];
      res.status(200).send(dataLast);
    });
}
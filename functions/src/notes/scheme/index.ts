const Joi = require('joi');

const createShema = Joi.object().keys({
  name: Joi.string().alphanum().required(),
  text: Joi.string().alphanum().required(),
  userId: Joi.string().required(),
  favorite: Joi.bool(),
});

const getLastSchema = Joi.object().keys({
  userId: Joi.string().required()
});

const updateSchema = Joi.object().keys({
  name: Joi.string().alphanum().required(),
  text: Joi.string().alphanum().required(),
  userId: Joi.string().required(),
  favorite: Joi.bool(),
});

const validateHelper = (data, schema, cb) => Joi.validate({ ...data }, schema, cb);

export {
  createShema,
  updateSchema,
  getLastSchema,
  validateHelper
}
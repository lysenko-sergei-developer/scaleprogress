import {
  createShema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.body
  }, createShema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    name,
    userId,
    favorite,
    text,
  } = req.body

  db.collection("notes")
    .add({
      name,
      userId,
      text,
      favorite: favorite ? favorite : false,
      createdAt: Date.now(),
      updateAt: Date.now(),
    })
    .then(notes => res.status(201).send(notes));
}
const express = require('express');
import db from "../firebaseStore";

const app = express.Router();

const {
  createSchema,
  validateHelper
} = require("./scheme");

import createUser from "./createUser";
import getUser from "./getUser";
import updateUser from "./updateUser";

app.get('/', (req, res) => getUser(db, req, res));
app.post('/', (req, res) => createUser(db, req, res));
app.put('/', (req, res) => updateUser(db, req, res));

export default app;
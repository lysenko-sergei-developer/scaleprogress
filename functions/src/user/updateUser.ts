import {
  updateSchema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.body
  }, updateSchema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    id,
  } = req.body
  const updatedBody = { ...req.body, updateAt: Date.now() };
  delete updatedBody.id;
  db.collection("users").doc(id).update(updatedBody);
}
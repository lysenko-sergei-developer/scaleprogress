const Joi = require('joi');

const createSchema = Joi.object().keys({
  firstName: Joi.string().alphanum().max(30).required(),
  lastName: Joi.string().alphanum().max(30).required(),
  photo: Joi.string(),
  points: Joi.number(),
  password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}$/).required(),
  email: Joi.string().email({ minDomainAtoms: 2 }).required(),
});

const getSchema = Joi.object().keys({
  password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}$/).required(),
  email: Joi.string().email({ minDomainAtoms: 2 }).required(),
});

const updateSchema = Joi.object().keys({
  id: Joi.string().alphanum().required(),
  firstName: Joi.string().alphanum().max(30),
  lastName: Joi.string().alphanum().max(30),
  photo: Joi.string(),
  points: Joi.number(),
  password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}$/).required(),
});

const validateHelper = (data, schema, cb) => Joi.validate({ ...data }, schema, cb);

export {
  createSchema,
  updateSchema,
  getSchema,
  validateHelper
}
import {
  createSchema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  validateHelper({ ...req.body
  }, createSchema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    firstName,
    lastName,
    email,
    password
  } = req.body
  db.collection("users")
    .add({
      firstName,
      lastName,
      email,
      password,
      points: 0,
      photo: "http://icons-for-free.com/icon/download-avatar_male_man_person_user_young_icon-329406.png",
      createdAt: Date.now(),
      updateAt: Date.now(),
    })
    .then(user => res.status(201).send(user));
}
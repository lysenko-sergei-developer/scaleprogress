import {
  getSchema,
  validateHelper
} from "./scheme";

export default (db, req, res) => {
  console.log(req.query)
  validateHelper({ ...req.query
  }, getSchema, (err, value) => {
    if (err) {

      res.status(400).send({
        status: "error",
        message: "Required fields are not filled"
      });
    }
  });

  const {
    email,
    password
  } = req.query

  db.collection("users")
    .where("email", "==", email)
    .where("password", "==", password)
    .get()
    .then((querySnapshot) => {
      const data = [];
      querySnapshot.forEach(doc => data.push({
        id: doc.id,
        ...doc.data()
      }));
      res.status(200).send(data);
    });
}